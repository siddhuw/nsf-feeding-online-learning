% !TEX root = main.tex

\section{Prior Work}
\label{sec:priorwork}

Our proposal draws from several interconnected areas of research on robot manipulation and machine learning.

\paragraph{Robot-Assisted Feeding: Food Manipulation.}
Several specialized feeding devices for people with disabilities have come onto the market in the past decade, some of which are shown in \figref{fig:feeding-devices}. Although there are several automated feeding systems in the market~\cite{obi, myspoon, mealmate, mealbuddy}, they have lacked widespread acceptance as they use minimal autonomy, demanding a time-consuming food preparation process~\cite{gemici2014learning}, or pre-cut packaged food and cannot adapt to large variations due to pre-programmed movements. 

Food manipulation, on the other hand, has been studied in the packaging industry~\cite{chua2003robotic, erzincanli1997meeting, morales2014soft, brett1991research, williams2001teaching, blanes2011technologies} with focus on the design of application-specific grippers for robust sorting and pick-and-place, as well as showing the need for visual sensing for quality control\cite{brosnan2002inspection, du2006learning, ding1994shape} and haptic sensing for grasping deformable food items without damaging them~\cite{chua2003robotic, erzincanli1997meeting, morales2014soft, brett1991research, williams2001teaching, blanes2011technologies}. Research labs have also explored meal preparation~\cite{ma2011chinese, sugiura2010cooking} as an exemplar multi-step manipulation problem, baking cookies~\cite{bollini2011bakebot}, making pancakes~\cite{beetz2011robotic}, separating Oreos~\cite{oreovideo}, and preparing meals~\cite{gemici2014learning} with robots. Most of these studies either interacted with a specific food item with a fixed manipulation strategy~\cite{bollini2011bakebot, beetz2011robotic} or used a set of food items for meal preparation which required a different set of manipulation strategies~\cite{gemici2014learning}.

\begin{figure*}[t!]
	\centering
	\begin{subfigure}[b]{0.24\textwidth}
		\centering
		\includegraphics[height=0.65\textwidth,keepaspectratio]{figs/mealbuddy.jpg}
		\caption{Meal Buddy}
		\label{fig:mealmate}
	\end{subfigure}
	\begin{subfigure}[b]{0.24\textwidth}
		\centering
		\includegraphics[height=0.65\textwidth,keepaspectratio]{figs/myspoon.jpg}
		\caption{My Spoon}
		\label{fig:mealmate}
	\end{subfigure}
	\begin{subfigure}[b]{0.24\textwidth}
		\centering
		\includegraphics[height=0.65\textwidth,keepaspectratio]{figs/obi.jpg}
		\caption{Obi}
		\label{fig:mealmate}
	\end{subfigure}
	\begin{subfigure}[b]{0.24\textwidth}
		\centering
		\includegraphics[height=0.65\textwidth,keepaspectratio]{figs/mealmate.jpg}
		\caption{Meal-Mate\textsuperscript{TM}}
		\label{fig:mealmate}
	\end{subfigure}
	\caption{Existing commercial feeding devices}
	\label{fig:feeding-devices}
\end{figure*}

Existing autonomous robot-assisted feeding systems such as \cite{2019Feng}, \cite{2019Gallenberger}, \cite{2016Park}, and \cite{herlant_thesis} can acquire a fixed set of food items and feed people, but it is not clear whether these systems can adapt to very different food items that require completely different acquisition and transfer strategies.
Feng {\em et al.} \cite{2019Feng} and Gordon {\em et al.} \cite{gordoncontextual} developed an online learning framework using the SPANet network and showed \emph{acquisition} generalization to previously-unseen food items, but did not address the problem of \emph{bite transfer} (See \figref{fig:results}). 
Gallenberger {\em et al.} \cite{2019Gallenberger} showed the existence of a relationship between bite acquisition and transfer, but did not propose how to learn a feeding system in such a setting. 
Our proposal aims to close this gap between bite acquisition and transfer in food manipulation work by developing a general online learning framework for robot-assistive feeding which generalizes to previously-unseen food items.

\begin{figure*}[t!]
	\centering
	\begin{subfigure}[b]{0.32\columnwidth}
		\includegraphics[width=\linewidth]{figs/ic_ooc_bar.pdf}
		\vspace{-0.05cm}
	\end{subfigure}
	\begin{subfigure}[b]{0.67\columnwidth}
		\includegraphics[width=\linewidth]{figs/fig_outclass.pdf}
	\end{subfigure}
	
	\iffalse
	\begin{table}[]
		\centering
		\begin{tabular}{|c|}
			\hline
			\textbf{In-Class} \\ \hline
			$0.81 \pm 0.02$ \\ \hline\hline
			\textbf{Out-Of-Class} \\ \hline
			$0.636 \pm 0.008$ \\ \hline
		\end{tabular}
	\end{table}
	\fi
	
	\caption{SPANet's \cite{2019Feng} generalizability to unseen food items (Out-Of-Class) compared with known food items (In-Class). Each bar represents the expected success rate of SPANet's proposal action, i.e., how well the best action proposed by SPANet would have performed according to the ground-truth data. \emph{Left:} Overall comparison. The success rate of SPANet's proposal action drops by 18\% for unseen food items.
		\emph{Right:} Per class comparison. For food classes from celery to cantaloupe, SPANet's OOC predictions stay very close to IC predictions but for items such as banana which has very different action distribution, the performance is below par. 
	}
	\label{fig:results}
	\vspace{-0.3cm}
\end{figure*}

%\subsection{General Bite Acquisition with Batch Learning}
% Describe SPNet, large 17x17x18 action space, significant improvemetn in success rate
%Tackling the problem of bite acquisition of arbitrary solid food items with a fork, Gallenberger {\em et al.} \cite{2019Gallenberger} separated perception from action selection. RetinaNet \cite{2017Lin} is used to segment out individual food items, and a second network, SPNet \cite{2019Gallenberger}, takes the cropped image and recommends a skewering location on a 17x17 grid and one of 18 roll angles, effectively selecting one of 5202 discrete actions. One limitation of this approach was the sparse utility of the action space, reducing both the data efficiency of the supervised learning and the ability of the network to generalize to previously-unseen food categories (as shown in Figure \ref{fig:spnet}). Improvement in acquisition success rates relative to a baseline strategy of simple vertical skewering at the center of bounding box stemmed from one of three sources. For longer foods (such as carrot and celery sticks), SPNet was trained to orient the fork tines perpendicular to the major axis of the food item, which reduced the likelihood of the food rolling away. For more slippery foods (such as strawberries and bananas), tilting the fork to the expert-defined pitches discussed below significantly improved the success rate independently of SPNet. For heterogenous food categories such as halved hard-boiled eggs, skewering simultaneously the white and the yolk resulted in stable lift off.

% Ongoing work: Describe SPANet: Further separate perception (incl. food position classification) from action-selection, define a smaller action space in terms of the food's orientation.
%A follow-up work by Feng {\em et al.} \cite{2019Feng} attempts to address bite acquisition of previously-unseen food items by directly predicting actions from food item bounding boxes and reducing the action space. The modified network, SPANet, returns the expected acquisition success rate for just 6 skewering actions: one of 2 roll angles (fork tines perpendicular or parallel to the major axis of the bounding ellipse of the food item) and one of 3 pitches (``vertical'', ``tilted-vertical'', or ``tilted-angled'', as described in Figure \ref{fig:strategies}). Generalization performance improved relative to SPNet, with food items like carrots and celery performing just as well when excluded from the training set.

\paragraph{Contextual bandits.}
Bandit algorithms have seen widespread success in online advertising \cite{Tang2013,Bottou2013}, health interventions \cite{Klasnja2015,Hochberg2016}, clinical trials \cite{Shortreed2011}, adaptive routing \cite{Awerbuch2004}, education \cite{Mandel2014}, music recommendations \cite{Wang2014}, financial portfolio design \cite{Shen2015}, or any application requiring a more optimized variant of multiple hypothesis testing. Adoption in robotics has been more limited, such as trajectory selection for object rearrangement planning \cite{Koval2015}, kicking strategies in robotic soccer \cite{Mendoza2016},  and perhaps most closely related, selecting among deformable object models for acquisition tasks \cite{McConachie2017}. We argue it is untenable to construct deformable object models for every food item, as conventional grocery stores typically stock in excess of 40,000 products \cite{Malito2017}. Instead, we take a model-free approach which operates directly on the image and haptic context spaces.

No-regret algorithms for solving bandit problems include UCB \cite{Auer2002} and EXP3 \cite{Auer2002} for stochastic and adversarial reward distributions, respectively. This was also extended to the bandits-with-expert-advice setting (a generalization of the contextual bandit problem for small policy classes) in EXP4 \cite{Auer2002}. Baseline methods for the contextual bandit problem include epoch-greedy \cite{Langford2008} and greedy \cite{Bastani2017}, both of which are simple to implement, perform well in practice, although do not achieve optimal regret guarantees. More recent advances include LinUCB \cite{Li2010}, RegCB \cite{Foster2018} and Online Cover \cite{Agarwal2014}, a computationally efficient approximation to an algorithm which achieves optimal regret. For a recent and thorough overview, see \cite{Lattimore2019}. Although our problem can be cast into the contextual bandit framework, there are aspects unique to food manipulation (e.g. delayed haptic feedback, costly expert queries) which require extending existing algorithms.




\paragraph{Results from Prior NSF Support.}
Srinivasa was funded by \textit{SCH: EXP: Collaborative Research: A Formalism for Customizing and Training Intelligent Assistive Devices}, 1R01EB019335-01, \$686,956, 09/01/14 - 8/31/2017, an NSF Smart and Connected Health grant funded as an R01 by the NIH. \textbf{Intellectual merit.}
The grant focused on assistance that augments control signals from the human to bridge the gap in control signal dimensionality for a variety of devices including powered wheelchairs and robot arms, and on customizing system components based on user data that can generalize to new situations. \textbf{Broader impacts.} The grant funded 2 PhD students and 1 undergraduate student at CMU and 1 PhD student at NU. Two of those students were women, and the research performed under the grant enabled them to win the Hertz and NSF Fellowships, and the CRA Outstanding Undergraduate Female Researcher Finalist award. The work also was featured at interactive National Robotics Week demos at CMU and at Chicago's Museum of Science
and Industry, with thousands of visitors (primarily K-12 children).



