% !TEX root = main.tex
\section{An Online Contextual Bandit Approach to Assistive Feeding}
\label{sec:framework}

We believe that adapting online contextual bandit methods for automated bite acquisition and transfer will lead to 
a natural dining experience by directly optimization for user feedback.
We argue an online learning approach is necessary due to (a) covariate shift between laboratory and real world user experiments and (b) the expensive process of collecting data on a physical robot.
Factors which may contribute to covariate shift include changing lighting conditions, backgrounds, and unknown a priori user transfer and food preferences. 

The need for online learning in robot-assistive feeding is clear even when examining bite acquisition and transfer success rates across a small set of food items in a controlled laboratory setting. For bite transfer, Gallenberger et al. \cite{2019Gallenberger} showed participant preferences for transfer strategy varied across food items. For example, carrot and celery items were preferred in the ``angled-transfer'' strategy, and cantaloupe were preferred in the ``horizontal-transfer'' strategy.

For bite acquisition, SPANet~\cite{2019Feng} is unable to generalize to some previously-unseen food categories (specifically kiwi and banana), as seen in \cite{2019Feng}. We hypothesize this lack of generalization is due partly to the high diversity of actions for these food categories. For example, the most successful action for kiwi and banana was ``tilted-angled'', which is very different from the rest of the food item dataset. To determine whether collecting additional training data would solve this problem, we controlled for the number of training examples. The results, shown in \figref{fig:data_amount}, do not show a noticeable improvement in out-of-class performance. Finally, it is well known that collecting data on a physical robot is expensive and time-consuming. The dataset used to train SPANet took approximately 81 hours of supervision. 

An online learning scheme allows for the system to leverage data collected in real-world conditions and adapt to each user's specific preferences. Therefore, in this work, we propose an online learning framework for this problem setting and present multiple algorithms based on the contextual bandit literature that could provide potential solutions.

\begin{figure}
	\centering
	\begin{tikzpicture}[auto,node distance=3cm]
	\node(s0)[draw, ellipse] {$\mathcal{S}_\textup{acquire}$};
	\node(s1)[draw, ellipse, right of=s0, node distance=4cm] {$\mathcal{S}_\textup{transfer}$}
	edge[<-] node[auto] {$c_{t, \textup{acquire}}=0$} (s0.east);
	\node(sf)[above of=s0, node distance=1.5cm] {}
	edge[<-] node[auto] {$c_{t, \textup{acquire}}=1$} (s0.north);
	\node(sf)[above right of=s1, node distance=2cm] {}
	edge[<-] node[auto] {$c_{t, \textup{transfer}} \in [0, c_\textup{max}]$} (s1);
	\end{tikzpicture}
	\caption{The bite acquisition and transfer protocols as a Markov Decision Process. Arrows without a terminus are terminal.}
	\label{fig:rl}
\end{figure}

\subsection{Problem Formulation}
On a high level, we consider a plate consisting of multiple discrete food items. In the case of continuous items (e.g. mashed potatoes), we assume the existence of an algorithm that can logically segment the item into discrete, bite-sized pieces. %If a food item is acquired, it will be immediately replaced, such that there are always $N$ food items on the plate.

We begin by making a simplifying assumption
\begin{assumption} \label{ass:independence}
For any successful bite acquisition, there exists at least one proceeding successful transfer strategy.
\end{assumption}
This allows us to decompose the problem into two independent contextual bandit problems. We hypothesize this assumption will decrease the required sample complexity at the expense of an asymptotic sub-optimality gap, a trade-off potentially worth making in initial experimentation. We explore this assumption in greater detail in \quesref{q:rl}.

At each round $t=1, \dots, T$, the \emph{bite acquisition} interaction protocol consists of
\begin{enumerate}
	\item{\em Context observation} The user selects a food item to acquire via an interface appropriate for their impairment, perhaps in combination with a separate object instance segmentation network. We observe the resulting RGB image with size $h\times w$ containing the single desired food item. We pass the image through SPANet, and use the penultimate layer as the context features $ x_{t, \textup{acquire}} \in \mathcal{X}_\textup{acquire}$.
	\item{\em Action selection} The algorithm selects one manipulation strategy $a_{t, \textup{acquire}} \in \mathcal{A}_\textup{acquire} = \{1, 2, \dotsc, K_\textup{acquire}\}$. In our initial implementation, $K_\textup{acquire}=6$, with 3 pitch angles and 2 locations.
	\item{\em Partial loss observation} The environment provides a binary loss $c_{t, \textup{acquire}} \in \{0, 1\}$, where $c_{t, \textup{acquire}} = 0$ corresponds to the robot successfully acquiring the single desired food item of a size appropriate for transfer to the user.
\end{enumerate}

Likewise, the \emph{bite transfer} interaction protocol consists of
\begin{enumerate}
	\item{\em Context observation} We observe an RGB image with size $h\times w$ containing the single acquired food item on the fork, and use the same image feature representation as in the bite acquisition phase. Additionally, we observe the haptic feedback from the acquisition phase. We concatenate these feature vectors into $x_{t, \textup{transfer}} \in \mathcal{X}_\textup{transfer}$. 
	\item{\em Action selection} The algorithm selects one transfer strategy $a_{t, \textup{transfer}} \in \mathcal{A} = \{1, \dotsc, K_\textup{transfer}\}$. In our initial implementation, $K_\textup{transfer}=2$ (``horizontal-transfer'' and ``angled-transfer'').
	\item{\em Partial loss observation} We ask the participants to take a bite and rate how easy it was to take the bite off the fork on a scale from $1-5$. We translate these scores into a loss $c_{t, \textup{transfer}} \in [0, c_\textup{max}]$, where $c_\textup{max}$ trades off the relative benefit between successful acquisition and transfer. The recipients could either eat the food or bite it off the fork and discard it.
\end{enumerate}
This is summarized in \cref{fig:rl}. Without \assref{ass:independence}, the problem becomes a two-step episodic reinforcement learning problem, a challenge we explore in \quesref{q:rl}.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.54\linewidth]{figs/strategies.png}
	\includegraphics[width=0.44\linewidth]{figs/data_amount.pdf}
	\caption{(Left) Action space for SPANet \cite{2019Feng}. From left-to-right: \emph{Tilted-Angled (TA)}: fork handle is oriented $45\deg$ relative to the plane of the table, \emph{Vertical (VS)}: fork handle is orthogonal to the table, and \emph{Tilted-Vertical (TV)}: fork tines are orthogonal to the table. (Right) SPANet out-of-class success rate using data from \cite{2019Feng}, given different amounts of data included in the training dataset. Each black line represents a single food item excluded from the training set. The red line represents the performance averaged across all food items. The takeaway here is that the amount of out-of-class training data has already reached the point of diminishing returns.}% \mbnote{We should see a drop in performance near 0\%, and asymptotically converging performance as we approach 100\%. The training data should be doing \emph{something}, and we want to argue we've already hit the point of diminishing returns.}}
	\label{fig:data_amount}
\end{figure}

The algorithm itself will consist of two stochastic policies $\pi_\textup{acquire}(x_t) = \mathbb{P}(a_{t, \textup{acquire}} = a | x_{t, \textup{acquire}})$ and $\pi_\textup{transfer}(x_t) = \mathbb{P}(a_{t, \textup{transfer}} = a | x_{t, \textup{transfer}})$, and the goal is to minimize the cumulative regret of each policy independently. In other words, we wish to minimize $R_T$, which is the difference in performance between our policy $\pi$ and the best possible policy $\pi^*\in\Pi$ for the lifetime of our program $T$. With $c_t\in\mathcal{C}$, $x_t\in\mathcal{X}$, $(a_t)\in\mathcal{A}$ at time $t$:
\begin{align*}
R_T := &\sum_{t=1}^T c_t(\pi(x_t)) - \min_{\pi^*\in\Pi}\sum_{t=1}^T c_t(\pi^*(x_t))
\end{align*}

As we are only able to execute one action on the robot at any time, and the action will alter or remove the food item, we only ever receive partial (or {\em bandit}) feedback. In other words, when our algorithm takes an action to pick up or transfer a food item, it can only see whether it has failed or succeeded with that action. It is not privy to the rewards of other actions. This naturally falls into the contextual bandit framework, where we expect the contexts to shift over time due to changing environmental conditions, user preferences and preceding acquisition actions (i.e.\ adversarial contexts) but the underlying costs remain stochastic. %The structure of this environment in relation to an arbitrary contextual bandit algorithm is presented formally in Algorithm \ref{alg:general}.

%	\begin{algorithm}[t]
%		\caption{General Contextual Bandit with SPANet Features}
%		\label{alg:general}
%		\textbf{Input:} Trained SPANet $\phi$, Environment $E$\\
%		\textbf{Initialize} $N$ Contexts $x \in \mathcal{X} \sim E$\\
%		\For{$t=1, \dotsc, T$}{
%			Find features $\phi(x)$\\ 
%			$p_t$ $\leftarrow$ {\tt explore}($\phi(x)$)\\
%			Select food item and action $(n_t, a_t) \sim p_t$\\
%			Receive $c_t \sim E | n_t, a_t$\\
%			{\tt learn}($\phi(x)$, $n_t$, $a_t$, $c_t$, $p_t$)\\
%			\eIf{$c_t = 0$}{
%				Re-sample context $x_{n_t} \sim E$
%			}{
%				continue
%			}
%		}
%	\end{algorithm}

\subsection{Online Learning using Contextual Bandits}
Contextual bandit algorithms can be constructed either through a reduction to importance-weighted reduction, or using estimators of the full loss with a reduction to cost-sensitive-classification. Either approach leverages an {\em oracle} to perform the underlying online supervised learning. All algorithms presented in the overview by Biette {\em et al.} \cite{Bietti2018} follow this general structure.

\paragraph{Reduction to Importance-Weighted Linear Regression}

For our feature extractor, we are using the activations of the penultimate layer in SPANet, and fine tune the final layer in an online fashion. Thus, we assume a linear map from the features $x_{t, \textup{acquire}}$ or $x_{t, \textup{transfer}}$ to the expected cost of each action: $\mathbb{E}[c_t|a_t,x_t] = \theta_{a_t}^Tx_t$. In this case, the regression oracle computes a weighted least-squares estimate $\widehat{\theta}$:
\begin{align*}
\widehat{\theta} := \sum_{t=0}^T\frac{1}{p_t(a_t)}\left(\theta_{a_t}^Tx_t - c_t(a_t)\right)^2
\end{align*}
Similarly to inverse propensity-scoring \cite{Bietti2018}, the weight $p_t(a_t)^{-1}$ ensures that this returns an unbiased estimate of the underlying true weights $\theta^*$. %An implementation of this oracle is shown in Algorithm \ref{alg:iwr}. %The policy associated with a given weight estimate $\widehat{\theta}$ will just be the greedy policy: $\pi_{\theta}(x) = \arg\min_{(n, a)}\theta_{a}^T\phi_n(x)$

%	\begin{algorithm}[t]
%		\caption{Importance-Weighted Regression Oracle}
%		\label{alg:iwr}
%		\SetKwFunction{FOracle}{oracle}
%		\SetKwProg{Fn}{Function}{:}{}
%		\textbf{Input:} Regularization parameter $\lambda$, $d=2048$ (features)\\
%		\textbf{Initialize} $\forall a \in \mathcal{A}$: $A_a\leftarrow \lambda I_{d\times d}; b_a\leftarrow 0$ \\
%		\SetKwProg{Pn}{Function}{:}{\KwRet}
%		\Pn{\FOracle{$\pi$, $\phi_{n_t}(x)$, $a_t$, $c_t$, $p_t(a_t)$}}{
%			$(A, b) \leftarrow \pi$\\
%			$A_{a_t} \leftarrow A_{a_t} + \frac{1}{p_t}\phi_{n_t}\phi_{n_t}^T$ \\
%			$b_{a_t} \leftarrow b_{a_t} + \frac{c_t}{p_t}\phi_{n_t}$ \\
%			$\widehat{\theta}_{a_t} \leftarrow A_{a_t}^{-1}b_{a_t}$ \\
%			$\pi' \leftarrow (\widehat{\theta}, A, b)$
%		}
%	\end{algorithm}

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.49\linewidth]{figs/transfer_strategies.png}
	\includegraphics[width=0.49\linewidth]{figs/transfer_results.png}
	\caption{(Left) Combinations of acquisition and transfer strategies. Transfer strategies are \emph{Angled-Tines (AT)}: fork handle is oriented oriented $45\deg$ relative to the plane of the face, and \emph{Horizontal-Tines (HT)}: fork handle is orthogonal to the face. VS is the \emph{vertical-skewering} acquisition action and AS is the \emph{angled-skewering} acquisition action which is also called the \emph{tilted-angled} action. (Right) Rankings for different transfer and acquisition strategy combinations.}
	\label{fig:transfer_strategies}
\end{figure}

\paragraph{Baseline Exploration Methods}

This simplest approach to this problem is the $\epsilon$-greedy algorithm. This algorithm opts for the optimal action $\pi_{\theta}(x) = \arg\min_{(n, a)}\theta_{a}^Tx$ based on previous observations with probability $(1-\epsilon)$ and explores all actions uniformly with probablity $\epsilon$. We propose the use of two variants of this algorithm: purely greedy ($\epsilon=0$) versus exploratory ($\epsilon > 0$).% and random food selection versus fixed food selection (where the same food item is used in all trials until it is removed from the plate).

With arbitrary contexts, the $\epsilon$-greedy algorithm (with optimized $\epsilon$) has a cumulative regret bound $R_T \sim O(T^{2/3})$, though it can perform well empirically \cite{Bietti2018}. The fixed variant may also provide a better regret bound, as it can provide better-than-bandit feedback for a given context by taking multiple actions.

%	\begin{algorithm}[t]
%		\caption{$\epsilon$-greedy baselines}
%		\label{alg:greedy}
%		\SetKwFunction{FExplore}{explore}
%		\SetKwFunction{FLearn}{learn}
%		\SetKwProg{Fn}{Function}{:}{}
%		\textbf{Input:} Exploration parameter $\epsilon\in[0,1)$\\
%		\textbf{Initialize} $n=\text{\tt rand}(1, N)$, $\pi_0$ \\
%		\SetKwProg{Pn}{Function}{:}{\KwRet}
%		\Pn{\FExplore{$\phi(x)$}}{
%			(random variant) $n = rand(1,N)$\\
%			$p_t(n,a) \leftarrow \frac{\epsilon}{K} + (1-\epsilon)\1\{\pi_t(\phi_{n}(x))\}$ %\xmnote{Changed $p_t(a)$ to $p_t(n,a)$}
%			%\xmnote{Return $p_t$ here?}
%		}
%		\Pn{\FLearn{$\phi(x)$, $n_t$, $a_t$, $c_t$, $p_t$}}{
%			$\pi_{t+1} =$ {\tt oracle}($\pi_t$, $\phi_{n}(x)$, $a_t$, $c_t$, $p_t(a_t)$)\\
%			(fixed variant) {\bf if} $c_t = 0$ {\bf then} $n = rand(1,N)$
%		}
%	\end{algorithm}

\paragraph{LinUCB Exploration}
\label{sec:linucb}
Given the linear regression function, we propose the use of LinUCB \cite{Li2010} due to its applicability to the setting of adversarial contexts, stochastic rewards and an infinite policy class. Each time step, we find the upper confidence bound (UCB) for each action. Treating the UCB as the cost for a given action implicitly encourages exploration, as in a choice between two actions with similar expected cost, the algorithm will opt for the one with higher variance. %It is worth pointing out that we maintain different $\theta_a$'s for each action and observe one feature vector  $\phi(x)$, while in \cite{Chu2011}, one $\theta$ is updated and $K$ feature vectors are observed. This difference, although caused by different problem setting between us and Chu {\em et al.} \cite{Chu2011}, should not affect the mechanism of the algorithm, as similar regret bound should hold in either settings.

%A single-food-item variant is shown in Algorithm \ref{alg:single}. Each round, the food item is fixed, but an extra action can be made to switch to a random food item with fixed cost $\gamma$. This covers the case where all actions on the current food item are expected to perform poorly. We also propose a multi-food-item variant in Algorithm \ref{alg:multi}. Here, exploration between food items happens implicitly by optimizing over the entire $N\times K$ action space.

With arbitrary contexts, LinUCB has a cumulative regret bound $R_T \sim O(T^{1/2})$, which is an improvement over $\epsilon$-greedy in the worst case. Like $\epsilon$-greedy, the fixed set of contexts may improve this bound.

%	\begin{algorithm}[t]
%		\caption{Single-food LinUCB}
%		\label{alg:single}
%		\SetKwFunction{FExplore}{explore}
%		\SetKwFunction{FLearn}{learn}
%		\SetKwProg{Fn}{Function}{:}{}
%		\textbf{Input:} Skip parameter $\gamma$; Width parameter $\alpha$\\
%		\textbf{Initialize} $n=\text{\tt rand}(1, N)$ \\
%		\SetKwProg{Pn}{Function}{:}{\KwRet}
%		\Pn{\FExplore{$\phi(x)$}}{
%			\For{$a\in\mathcal{A}$}{
%				$ucb_a \leftarrow \theta_a^T \phi(x)+ \alpha \sqrt{\phi(x)^T A_a \phi(x)} $\ \cite{Chu2011}\\
%			}
%			% 	\For{$a\in\mathcal{A}$}{
%			% 	    $C_a \leftarrow \{\widehat{\theta}_a : ||\widehat{\theta}_a - \theta^*_a|| \leq \epsilon\}$\footnotemark\\
%			% 	    $lcb_a \leftarrow \min_{\theta_a\in C_a}\phi_n^T\widehat{\theta_a}$
%			% 	}
%			\eIf{$\min_a ucb_a \le \gamma$ and not first action on $n$}{
%				$n=\text{\tt rand}([1, N]\setminus n)$\\
%				{\tt explore}($\phi(x)$)\\
%			}{
%				$p_t(n', a') \leftarrow \1\{a' = \arg\max_a ucb_a\ ,\ n' = n\}$\\
%			}
%		}
%		\Pn{\FLearn{$\phi(x)$, $n_t$, $a_t$, $c_t$, $p_t$}}{
%			$\pi_{t+1} =$ {\tt oracle}($\pi_t$, $\phi_{n}(x)$, $a_t$, $c_t$, $p_t(a_t)$)\\
%		}
%	\end{algorithm}

%	\begin{algorithm}[t]
%		\caption{Multi-Item LinUCB}
%		\label{alg:multi}
%		\SetKwFunction{FExplore}{explore}
%		\SetKwProg{Fn}{Function}{:}{}
%		\SetKwProg{Pn}{Function}{:}{\KwRet}
%		\Pn{\FExplore{$\phi(x)$}}{
%			\For{$n\in [1,N]$, $a\in\mathcal{A}$}{
%				% $C_a \leftarrow \{\widehat{\theta}_a : ||\widehat{\theta}_a - \theta^*_a|| \leq \epsilon\}^{\text{1}}$\\
%				$lcb_{n, a} \leftarrow \min_{\theta_a\in C_{n,a}}\phi_n^T\widehat{\theta_a}$\\
%				$ucb_{n,a} \leftarrow \theta_a^T \phi_n+ \alpha \sqrt{\phi_n^T A_a \phi_n} $
%			}
%			$p_t(n', a') \leftarrow \1\{(n', a' = \arg\max_{n, a} ucb_{n, a}\}$\\
%		}
%	\end{algorithm}