% !TEX root = main.tex

\section{Evaluation Plan}
\label{sec:evalplan}

Our evaluation plan consists of extensive real-world evaluation of our online algorithm in real-world settings.

\paragraph{Robot Platform.} We will use the JACO robotic arm~\cite{campeau2016jaco} from Kinova Robotics. The JACO robotic arm that we have is a 6-DoF robotic arm with a 2-finger gripper. The JACO arm features torque, joint position sensing and out-of-the-box interfacing tools (C++ API and ROS driver). In developing the software for this platform, we will leverage our AIKIDO stack, DART physics engine~\cite{lee2018dart} and MAGI task-planning framework.

\paragraph{Perception.} We plan to augment the wheelchair-mounted Kinova JACO arm with a single RGBD sensor (e.g. Intel RealSense~\cite{keselman2017intel}) with a wireless perception module and an F/T sensor mounted on a utensil such as a fork, to sense the environment. To recognize objects, we will leverage a Faster R-CNN framework~\cite{ren2015faster}.

\paragraph{Planning.} In planning robot policies, our aim is to produce consistent and efficient motion. Our planning stack will consist of a slew of algorithms~\cite{srinivasa2016system} such as Snap planner, RRT, RRT-Connect, CBiRRT etc. going either to a goal configuration, following a constrained trajectory, or planning to a Task-space Region~\cite{berenson2011task}.

\subsection*{Experiment 1: Comparing variants of online learning algorithms for bite transfer.} \label{exp:transfer}
For this experiment, our goal is to compare online algorithms on a variety of previously unseen food items \emph{specifically for the bite transfer task}. In \cite{gordoncontextual}, we demonstrated the success of online learning for bite acquisition. This will allow us to explore user feedback reward functions, features, multimodal contexts, and action spaces unique to the transfer domain to address \quesref{q:contextspace} and \quesref{q:delayed}.

\paragraph{Hypothesis.}
Our hypothesis is that the performance of online algorithms after \emph{i} iterations would be significantly better than both (a) the current baseline system which always chooses the same transfer strategy and (b) an improved system which has been trained on previously seen food items, with $p<0.05$. The performance analyses with different rewards, context features, and action spaces are exploratory. However, we hypothesize that a compact feature representation and a combination of visual and haptic context would be significantly better than an online algorithm with our original feature representation and visual-only contexts with $p<0.05$.

\paragraph{Evaluation Metrics.}
We measure the quality of a bite transfer attempt by asking the participants to take a bite off the fork and answer the question ``How easy was it for you to eat the food item off the fork?'' on a scale from $1-5$. We would compare the success rates of these different algorithms.

\paragraph{Proposed Experiment Procedure.}
We evaluate these algorithms on previously-unseen food items such as carrots and celery with very different optimal transfer strategies from food items such as a strawberry, and thus difficult to generalize to. At one time, there is one food item on the fork and we manually replace the food item after it has been transferred to the user. We would repeat this bite transfer trial $10$ times. So, in total, there would be $2$ food items x $2$ algorithms (LinUCB and $\epsilon$-greedy) x $10$ trials. We will also compare the performance of our algorithms with various feature representations, action spaces, and combination of multiple sensing modalities such as vision and haptic feedback.

\subsection*{Experiment 2: Comparing online learning algorithms and action spaces with human baselines.} \label{exp:humanbaseline}
We will perform this experiment in two parts. For the first part, our goal is to compare the performance of online algorithms on previously unseen food items to a human's performance~\cite{2019Feng} on those food items to address \quesref{q:expertbaseline}. For the second part, our goal is to leverage feedback from the participants to revise the action space of our bite acquisition framework to be able to deal with a large set of food items with different physical characteristics to address \quesref{q:actionspace}. We will initially focus on bite acquisition to ease the user experiment burden.

\paragraph{Hypothesis.}
Our hypothesis is that there would not be significant difference between the performance of online algorithms and humans after \emph{i} iterations with $p<0.05$. 


\paragraph{Evaluation Metrics.}
We define a bite acquisition attempt as success if a target food item is on the fork for $5$ seconds after it is lifted off from the plate. We define a bite acquisition attempt as a failure if a target food item is skewered with at least 2 out of 4 tines but the fork fails to pick it up or the food falls off before 5 seconds after lift-off. If less than 2 out of 4 tines touch a food item due to other perception or planning related errors, we would discard the trial as it does not evaluate the performance of the algorithms in question. We would compare the success rates of the human baseline with the automated solution across various action spaces.

\paragraph{Proposed Experiment Procedure.}
Our experimental setup is exactly the same as the previous experiment. We will randomly assign participants to do either the first part or the second part of the experiment. For the first part of the human study, we propose to build a web interface which shows the same context (in this case, a bounding box image of a target food item) to the human as an online algorithm would see. The web interface also shows if the attempt was a success or a failure and displays all the $6$ actions that the participant can choose from during a particular bite acquisition attempt. During the experiment, the participant will be completely isolated from the rest of the robot setup to provide the same context that an online algorithm would actually get. Based on power analysis from a pilot study, we will determine the number of human participants for this study. 

For the second part, we will develop a different web interface with continuous parameterizations of the action space and its corresponding visualization. The participants will see the robot performing different actions on different food items. At the end of the robot skewering trials, we will ask the participants if they want to revise the action space of our bite acquisition framework to be able to deal with a large set of food items with different physical characteristics.


\subsection*{Experiment 3: Evaluating our online learning framework in autonomous and shared autonomy settings with end-users.}
For this experiment, our goal is to evaluate our learning framework with end-users. Specifically, we are interested in comparing the performance of a fully autonomous robot-assisted feeding system with the framework that leverages expert queries and user feedback in a shared autonomy setting to address \quesref{q:hil} and \quesref{q:feedback}.

\paragraph{Hypothesis.}
This is an exploratory study. However, we believe that an end user's preference towards a particular setting will be a function of the robustness of an autonomous system and its susceptibility to errors and the trade-off between having control with more required effort in a shared-autonomy setting. Further, we expect the use of some expert queries to favorably increase the bite acquisition and transfer performance, while requiring fewer expert queries than a non-learning baseline system.

\paragraph{Evaluation Metrics.}
We will evaluate this study using the widely known metrics of Technology Acceptance Model (TAMs)~\cite{davis1989perceived} in information systems theory. Based on this model, we will provide questionnaires to the end-users at the end of the study which will evaluate the performance of the system under the categories of \emph{Perceived Usefulness}, \emph{Perceived Ease of Use}, \emph{Attitude}, \emph{Intention to Use}, and \emph{Perceived Enjoyment}. Intermediate metrics will include the bite acquisition and transfer costs defined in Experiments 1 and 2.

\paragraph{Proposed Experiment Procedure.}
Our experimental setup for this experiment will consist of feeding a person with upper-body mobility impairments under two settings. In the autonomous setting, the robot will feed the person autonomously. In the shared autonomy setting, the robot will feed the person autonomously but during certain times will query an expert caregiver based on certain phases of feeding and use their feedback to complete the feeding process. For both the settings, the robot will use our online learning framework to design its behaviors.

\subsection*{Experiment 4: Contextual bandit vs. full reinforcement learning solution.}
The objective of this experiment is to empirically test the validity of \assref{ass:independence} and thus answer \quesref{q:rl}. We do this by comparing the dual contextual bandit formulation with an episodic reinforcement learning (RL) formulation, which allows planning potentially sub-optimal acquisitions which are better transfer strategies.

\paragraph{Hypothesis.}
An episodic reinforcement learning solution will decrease the total cost by trading off improved bite transfer for slightly reduced acquisition performance with $p<0.05$. The downside will be increased sample complexity for the RL algorithms.

\paragraph{Evaluation Metrics.}
We will examine the trade-off between the acquisition metric defined in Experiment 2 and the transfer metric defined in Experiment 1. Further, we will form a composite metric by summing these two costs to examine the trade-off between the total feeding cost (acquisition plus transfer) with the sample complexity of the contextual bandit and reinforcement learning frameworks.

\paragraph{Proposed Experiment Procedure.}
The contextual bandit baseline is the same as in previous experiments. We will conduct the same experimental procedure for several baseline model-free approximate dynamic programming RL algorithms and examine the trade-off between the total cost and sample complexity.

\begin{figure*}[t!]
	\centering
	\includegraphics[width=1\linewidth]{figs/acquisition_results.pdf}
	\caption{Experimental validation of LinUCB for bite acquisition using the Autonomous Dexterous Arm (ADA). The robot acquired previously-unseen under-ripe bananas for the first 27 trials, which are firm enough to be picked up with vertical skewers. For the next trials with \emph{ripe} bananas, the algorithm switched to the only viable action (tilted-angled) within 10 attempts. Attempts 50-55 were exclusively tilted-angled. Finally, we alternated between carrots (optimal action VS) and ripe banana (optimal action TA) to verify that the model did not over-fit.}
	\label{fig:acquisition_results}
\end{figure*}

\subsection*{Preliminary Findings}
\label{sec:prelim}

\emph{Bite transfer:}
We hypothesized that users would prefer different bite transfer strategies for different food items and that transfer strategy performance would vary across food items. Insight about the former was gained from a human study \cite{bhattacharjee2018food}, which found that some people, when tasked with feeding a mannequin, tilted the fork for specific food items in order to orient the food and allow for easy bite transfer.

To test the second hypothesis, we conducted a human study of 25 participants from 18-37 years of age. Angled skewering combined with angled transfer (AS-AT) resulted in significantly easier bite transfer than vertical skewering with horizontal transfer VS-HT or vertical skewering with angled transfer VS-AT for long food items. However, for smaller and more cubic-like shaped cantaloupes, the current experiment did not find statistically significant differences between any of three manipulation primitives. This implies that for small food items, a simple strategy like vertical skewering with horizontal transfer may work quite well. These results are summarized in \figref{fig:transfer_strategies}.

\emph{Bite acquisition: }
We tested the ability of LinUCB to adapt to previously-unseen food items by conducting bite-acquisition experiments on a real robot platform. The physical environment is identical to the one used by \cite{2019Gallenberger}. We used the doubly robust estimator to construct unbiased and robust estimates of the full expected loss.
%For  each  trial,  we  place  a  single  food item  in  the  center  of  the  plate.  
ADA  positions  itself  vertically  above  the  plate  and  performs  object detection, featurization,  and  action  selection  using  a  checkpoint  of SPANet  that  has  been  trained  on  15  food  items,  excluding banana.  After  performing  the  requested  action,  the  binary loss  is  recorded  manually  and  used  to  update  the  online learning  algorithm.  For  consistency,  regardless  of  success, we removed and replaced the food item after every attempt. We  first  conducted  27  trials  on  deliberately  under-ripe bananas. We hypothesized that, with the firmer texture, they would  be  relatively  easy  to  pick  up  with  most  actions, leaving  little  incentive  for  the  online  learning  algorithm  to explore  beyond  the  actions  that  generally  performed  well on  the  other  15  food  items  (VS  and  TV).  The  following 27  trials  were  conducted  with ripe bananas.  Previous  data suggested  that  acquisition  would  be  almost  impossible  for them  with  any  action  besides  TA,  making  them  the  most unique  item  in  our  dataset  and  therefore  the  most  resistant to generalization. Afterwards, we performed another 5 trials on a carrot (a previously-seen food item which requires VS to pick up consistently) followed by another 5 trials on ripe banana to test the online learning algorithm’s ability to retain its  knowledge  of  the  optimal  action  for  each  type  of  food without over-fitting. The identity of the food items was never made available to the online learning algorithm.

The number of times the online learning algorithm selected an action, cumulative across each food item, is presented in \figref{fig:acquisition_results}. The empirical success rate of VS and TV on the under-ripe banana was $33\%$ and $27\%$ respectively, not statistically significant from TA's \emph{a priori} success rate of $\sim30\%$ based on the previously-seen 15 food items. Therefore, as expected, we see the online learning algorithm primarily stick with VS and TV (with a slight bias towards VS), only beginning to explore TA late in trial 24.

The empirical success rate distribution abruptly flipped with the ripe bananas. VS and TV both had a success rate of 0 while TA exhibited an $83\%$ success rate. LinUCB began experimenting with TA after 7 trials, and after trial 20, it was almost exclusively choosing that action.
For the final 10 trials, the online learning algorithm demonstrated that it did not forget the optimal action for previously-seen food items. It performed the optimal action on carrot (VS, $90^\circ$ rotation) 5 times in a row, and when returning to the ripe banana, it performed the optimal action (TA, any rotation) 4 out of 5 times. These results suggest to us that contextual bandits with discrete, dissimilar actions is a promising route to data-efficient adaptive bite acquisition.
%$$\hat{l}_{DR}(x_i, a) = \hat{l}_a + (l_i - \hat{l}_a) \frac{\1(a_i = a)}{p(a_i|x_i)}$$

%where $\hat{l}_a$ is imputed loss by averaging binary losses over the food item and the action. $l_i$ is the loss for $i$th trial.  $p(a_i|x_i)$ is called the propensity score given the context $x_i$, which is equal to $1/6$ since the data in \cite{2019Feng} is collected using random policy. 


\subsection*{Timeline.}
Our research questions have a natural dependency structure about them that allows us to logically lay them out on a proposed timeline as shown in Table \ref{tbl:timeline} (q1= Quarter 1, q12 = Quarter 12).

\begin{table}[h]
	\centering
	\caption{Proposed Project Timeline}
	\begin{tabular}{l|llllllllllll}
		\toprule
		& q1    & q2    & q3    & q4    & q5 & q6 & q7 & q8 & q9 & q10 & q11 & q12 \\ 
		\toprule
		\multirow{4}{*}{M1} &      \multicolumn{4}{|c|}{RQ1}               &            &            &        &        &     &         &         & \\       
		&            &            &            &            &     \multicolumn{3}{|c|}{RQ2}     &         &           &    &      &         \\
		&     \multicolumn{2}{|c|}{RQ3}          &            &            &            &         &         &         &    &  &    &         \\
		&            &            &            &            &         &         &         &  \multicolumn{3}{|c|}{RQ4}  &    &                \\
		\toprule
		\multirow{5}{*}{M2} & \multicolumn{2}{c|}{RQ5} &            &            &         &         &         &         &         &  & & \\      
		&              &      \multicolumn{4}{|c|}{RQ6}          &         &         &         &         &         &         &         \\
		&            &            &  &         &         &     \multicolumn{3}{|c|}{RQ7}          &         &         &         &         \\
		%&            &            &            &        &         &     \multicolumn{3}{|c|}{RQ8}    &         &         &         &          \\
		&            &            &            &        &     &         &         &          &   \multicolumn{4}{|c|}{RQ8}          \\
		\bottomrule
	\end{tabular}\label{tbl:timeline}
\end{table}
