% !TEX root = main.tex

\section{Introduction}
\label{sec:intro}

Nearly $56.7$ million (18.7\%) of the non-institutionalized US population had a disability in 2010~\cite{brault2012americans}. Among them, about $12.3$ million needed assistance with one or more activities of daily living (ADLs), such as feeding, bathing, or dressing. Indeed, needing help with one or more of ADLs is the most cited reason for moving to assisted or institutionalized living~\cite{Mlinac2016, agisLiving}. Moreover, depression and disability can worsen after ADL assistance is initiated, perhaps due to associating helplessness with care on the part of the care recipient or loss of regular activity that could mitigate further disability~\cite{LinAndWu2011}. The ability to self feed would not only save time for caregivers, but also increase a person's sense of self worth \cite{1990Prior}, \cite{1994Stanger}. While some commercial feeding systems exist \cite{obi}, \cite{myspoon}, these systems have minimal autonomy requiring pre-programmed movements, thus making it difficult to adapt to specific user preferences and changing environments. 

In this work, we propose a general framework based on contextual bandits which allows directly optimizing for user preferences in an online fashion. 
We specifically focus on the assistive self-care robotic tasks of bite acquisition and bite transfer, both of which fit into this framework \cite{bhattacharjee2018food}.
Different food items may require different strategies \cite{bhattacharjee2018food} and a robust system needs to be able to acquire and transfer these myriad types of food items that each user might want to eat. While we have achieved some recent successes in developing manipulation actions that can \emph{acquire} a variety of food items \cite{2019Feng,2019Gallenberger,gordoncontextual}, a key additional challenge is to \emph{transfer} previously-unseen food items that have very different action distributions. Our key insight is that by leveraging contexts from previous bite dining experiences, an autonomous system should be able to learn online how to both acquire and transfer these previously-unseen food items. We formalize both bite acquisition and transfer into the online contextual bandit framework. 
We consider research questions around different context representations and leveraging costly user feedback by extending baseline $\epsilon$-greedy and LinUCB algorithms.
We believe that using this proposed research direction, we can not only take a significant step towards solving this highly impactful real-world application but also can use these research questions to explore generic solutions towards robust and generalizable non-prehensile manipulation of hard-to-model objects and the nuances of shared autonomy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Our key technical insight is ...
Our proposal is based on this central tenet:
\begin{quote}
By leveraging user feedback and contexts from previous feeding attempts, a robot should be able to learn online how to adapt to new food items, users and environments.
\end{quote}


We are excited about directly optimizing for the user experience through user feedback and formalizing this into the contextual bandit framework.
However, there are several challenges, both \emph{algorithmic} and \emph{practical}. 

Algorithmically, we propose to address three challenges: (1) trading off costly expert queries with automated attempts through a human-in-the-loop system (2) choosing powerful and efficient context representations, and (3) leveraging delayed contextual information. 

Practically, we propose to leverage our experience in building and fielding real-world robotic systems to \emph{demonstrate} our framework on a wheelchair-mounted robotic arm called ADA in the homes of people with mobility-impairments.

We provide a formal mathematical description of our framework in \sref{sec:framework}. We discuss in detail the research challenges and our proposed solutions in \sref{sec:questions}, and a detailed evaluation plan for quantitative assessment on real-world systems in \sref{sec:evalplan}, as well as outline some of our encouraging preliminary work. 


\paragraph{Intellectual Merit}
%Intelligent assistive technologies require both meaningful user feedback and rich contextual information. 
The proposed research direction allows for incorporating learning into an assistive technology to adapt to diverse user preferences and environments. The specific research questions will advance our understanding about integrating complex user experiences and social environments into a coherent machine learning robotic system. We believe this constitutes a rich domain of directly optimizing for quantitative user experiences, and specifically the ability of machine learning techniques to adapt to changing social and environmental factors in an online fashion. 

\begin{itemize}
\item Q1 \textbf{Leveraging User Feedback} How can we directly and efficiently optimize for the user experience? A fundamental challenge for machine learning in CHS is to accurately predict individual user preferences. The proposed contextual bandit framework efficiently balances learning user models with choosing strategies which are ranked highly during the user studies. We will formalize the trade-off between costly human expert queries and automated attempts within the contextual bandit framework.
\item Q2 \textbf{Exploring Different Contexts} How should we represent and augment sensor information? We will empirically validate action and context space representations which are both powerful (i.e.\ high asymptotic success rate) and efficient (i.e.\ fast convergence). Such representations are necessary to quickly learn a high performing user model. We will contribute contextual bandit algorithms which generalize to inconsistent context space dimension.
\end{itemize}


\paragraph{Significance.}
We propose a reduction from the complex food acquisition and transfer domain into the contextual bandit framework, which allows efficient optimization for user preference feedback. We leverage powerful existing contextual bandit algorithms, which have seen widespread success in domains outside of CHS, especially in internet advertising \cite{Tang2013,Bottou2013}. This is pertinent given recent advancements in contextual bandit algorithms and deep learning techniques to process computer vision inputs. We believe contextual bandits, a highly optimized generalization of multiple hypothesis testing, has widespread potential in CHS to efficiently adapt computing systems to human needs in an online and user-specific fashion.

\paragraph{Relevance to CHS (\emph{emphasis} from program solicitation).}
%\tbnote{Need to revise this to better connect it with CHS}.
%\mbnote{This program solicitation? \url{https://www.nsf.gov/funding/pgm_summ.jsp?pims_id=504958}}
Our work is clearly motivated by the intent to \emph{develop a new computing system to amplify human's physical capabilities to accomplish individual goals.} We will design this \emph{assistive technology for persons with impairments} by deploying our algorithms on real robot hardware and extensively studying their performance with upper-arm mobility-impaired users in their homes.
%\emph{develop a smart system whose design methodologies are based on data collected from potential system users}. 
We address the \emph{science} of CHS by integrating the fields of robotic manipulation, human rehabilitation, control theory, machine learning, human-robot interaction, cognitive science, and human subject studies; and the \emph{individual factors impacting usability} of CHS by designing new models which directly optimize for user feedback preferences collected from human subject studies. Our efforts depend upon close connections between computer science and rehabilitation medicine departments at the University of Washington; the UW's Taskar Center for Accessible Technology (TCAT); industry partners such as Kinova Robotics; and one of Washington's largest non-profit agencies for meeting the needs of adults with disabilities, Provail.

% Our work is clearly \emph{synergistic}: at the intersection of robotic manipulation, human rehabilitation, control theory, machine learning, human-robot interaction and clinical studies. We address the \emph{science} of CPS by developing new models of the interaction dynamics between the system and the user, the \emph{technology} of CPS by developing new interfaces and interaction modalities with strong theoretical foundations, and the \emph{engineering} of CPS by deploying our algorithms on real robot hardware and extensive studies with able-bodied and SCI users.