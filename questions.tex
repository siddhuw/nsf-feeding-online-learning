% !TEX root = main.tex

\section{Research Challenges}
\label{sec:questions}

Our online contextual bandit framework for acquiring and transferring food items provides the foundation to address challenges in (M1) \emph{leveraging user feedback} to benchmark, learn, and develop methods for a natural dining experience, and (M2) \emph{exploring different contexts} for generalizing bite acquisition. We consider each of these milestones in turn:

\subsection*{M1 Leveraging User Feedback}

\begin{question} \label{q:expertbaseline}
	How does online learned bite acquisition and transfer compare to a costly expert baseline?
\end{question}
In some accessibility domains, it is common to use near real-time crowd-sourcing to label data. For example, image captioning mobile applications for the visually impaired send photos and a question from the user (acquired from the speech-to-text inferface, e.g. ``What temperature does this thermostat show?'') to a crowd-sourced worker for an answer (e.g.\ 71 degrees)  \cite{bigham2010vizwiz}. Machine learning methods have proven far less useful in this domain due to the complexity of the context and action spaces. Through this research question, we would like to explore how online human feedback performs when compared to our online learning framework in this bite acquisition domain. 

An approach for a online human feedback for bite acquisition would involve sending an image of the food item to an online worker, who chooses among the same set of actions available to the contextual bandit algorithm (e.g.\ ``tingled angled'' , ``tilted vertical'', ``horizontal transfer'', ``angled transfer''). However, we believe the bite acquisition domain is better-suited for automated methods than many other accessibility challenges due to (a) the smaller action space ($K_\textup{acquisition}=6$, $K_\textup{acquisition}=2$) and (b) an interface mismatch, i.e.\ the action selected by a human via a computer interface may be different than the action they would have naturally chosen when eating the food themselves. We propose to study this question by conducting a comparative study between the online learning solution and crowd-sourced workers. This study should provide us with invaluable insights about how fast can a human converge to a successful bite acquisition action when compared to an online learning algorithm with the same context.

\begin{question} \label{q:hil}
	Can we optimally trade-off autonomous bite acquisition with costly expert queries?
\end{question}
Our preliminary results demonstrate the robot's success at acquiring many types of food, and greater difficulty at acquiring rarely seen or difficult to manipulate food items. We propose a policy which is able to trade-off between when to directly choose an acquisition or transfer strategy vs. when to send the image to an online worker for additional context or a recommended action. This is unlike the typical bandits with expert advice setting (e.g.\ EXP4), which assumes each expert provides a recommended action distribution at every round. Here, we must choose when to query an expert due to some known costs (latency and labor). Further, one can imagine a range of experts to query, each with varying costs and levels of performance.

\begin{question} \label{q:actionspace}
	How should we revise our action spaces to acquire and transfer a wide range of food items?
\end{question}
Our current bite acquisition and transfer framework looks at skewering and transferring solid food items with small action spaces of $6$ and $2$ actions, respectively. However, the universe of food items is huge and it remains to be seen that given an unseen food item with a very different physical characteristic, if indeed it is possible to acquire and transfer these items using these actions which only vary by fork roll, pitch and location. Clearly, continuous food items such as mashed potato or rice, that need to be scooped or a spaghetti which may require twirling may prove to be challenging to acquire. Through this research question, we hope to revise the action space to enable picking up and transferring a wider range of unseen food item by observing humans. We propose to leverage our previously built taxonomy of food manipulation and conduct a study in which we provide humans the option to design actions to pick up a wide variety of food items using the same context that is available to the robot.

\begin{question} \label{q:feedback}
	When should an algorithm request additional feedback from a user?
\end{question}
Multimodal contexts can be very useful in getting more information about a food item. In addition to visual and delayed haptic contexts after skewering, an online learning algorithm should be able to leverage feedback from a user to learn more contexts for effective bite acquisition, e.g.\ ``Is this the center of the strawberry?''. The key challenge here is how to optimally trade off between performance and user effort or workload. If our algorithm is confident the current strategy will be successful, then asking the user a question is unnecessarily burdensome. Further, different types of information have varying levels of burden, e.g.\ a binary ``yes/no'' question could be less burdensome than selecting a location on a computer screen or inquiring about the physical properties of a food item.

We propose formulating this problem as an instance of adaptive feature selection (AFS) \cite{Nan2016,Janisch2017}. The key challenge is how to generalize the contextual bandit and AFS frameworks into a single, consistent framework.

\subsection*{M2 Exploring different contexts} \label{sec:rl}

\begin{question} \label{q:contextspace}
	What is the most powerful feature representation for designing contexts for online learning algorithms?
\end{question}
Deciding an appropriate context for the online learning algorithms is of paramount importance. However, it is not clear what is an appropriate feature representation for these contexts for bite acquisition and transfer. In our preliminary bite acquisition experiments, we used feature vectors in $\mathcal{X}_{\textup{acquire}} = \mathbb{R}^{2048}$, which was selected based on strong performance in standard batch learning tasks. However, this does not necessarily imply this is the best feature representation for online learning, where our primary goal is to quickly adapt to previously unseen food items. Theoretically, this is well motivated by the fact that the regret of LinUCB scales $\mathcal{O}(d\sqrt{T} log(T))$ where $d$ is the feature dimension and $T$ is the time horizon. Further, our preliminary empirical analysis required large amounts of regularization when $d=2048$ -- which suggests the need for a more compact feature space.

One possible solution to this problem is to encourage sparsity in the final layer of the network via an $\ell_1$ regularizer. The strength of the regularization would need to be determined such that an optimal trade-off between the dimensionality and batch learning performance is achieved.

\begin{question} \label{q:delayed}
	How can we incorporate delayed context in the form of haptic feedback for bite acquisition?
\end{question}
Bite acquisition of a food item involves visually perceiving a food item and then physically skewering it to pick it up. Therefore, during the bite acquisition phase, we are limited from using non-visual information. Haptic feedback provides rich information on the deformability of food items and could improve the acquisition success rate. However, haptic information is a form of \emph{delayed context}, which is available only after a first attempt is made at acquiring a food item. This is a challenge for the contextual bandit framework, which assumes the full context is observed prior to taking an action. 

Through this research question, we explore how we can adapt the typical stochastic bandit setting to include this additional, delayed context for bite acquisition? For simple cost-sensitive-classification or regression oracles (e.g.\ Na\"ive Bayes), it is possible to construct versions with variable sized inputs. For more general, powerful oracles, it may be necessary to learn a second instance which takes as input both the visual and haptic contexts. The fundamental algorithmic and theoretical challenge is how to properly incorporate both learners such that either the visual or visual and haptic context may be used. Given that the haptic interaction happens over time, an additional challenge is to find an appropriate feature representation for combining the visual and haptic contexts for training an online learning network with two possible sized contexts.

This is not true for the bite transfer phase, where both the haptic and visual contexts are always observed prior to choosing an action. We can readily empirically validate the benefit of each context component through an ablation study.

%\begin{question} \label{q:interaction}
%	Can we extend the contextual bandit setting to different modes of interaction?
%\end{question}
%The context for manipulating food items for feeding can also vary depending on the mode of interaction between the robot and the user. During a meal, a user can either request a specific piece of food item, a specific type of food item, or may not care what food item is being fed at a specific time. A user could also specify a food item based on some spatial relationship with respect to its immediate environment (another food item or the plate / bowl itself). We imagine multiple modes of interaction, depending on the desires of the user. 
%
%At one end of the spectrum, we have fine-grained control mode in which the user selects exactly which food item to acquire via an appropriate interface. This follows the standard contextual bandit setting, where at each round one context is presented to the policy, which chooses an action and receives a reward (i.e.\ success or failure). 
%
%In the other end of the spectrum of control modes, we free the user from selecting a food item, which may become tedious. A na\"ive strategy is to randomly select a food item. However, some food items may be easier to acquire due to their placement on the plate or the type of food item. We propose formalizing this setting by concatenating all the contexts into a single context -- thus actions correspond to both the food item and acquisition strategy. Fortunately, it is possible to maintain the same network architecture, which effectively shares parameters across the contexts and mitigates the increase in the dimensionality $d$. The control modes in between these two extremes can be dealt with as a variation of this mode by concatenating contexts depending on the region of interest.

\begin{question} \label{q:simulate}
	Can we explore simulation paradigms to provide a variety of contexts with full feedback for a subset of food items?
\end{question}

Food items are deformable and hard to model. Therefore, in real world once skewered, it is very challenging to create the same context for the same food item in a different acquisition attempt. Thus, our online learning algorithms have access to only partial feedback. However, it may be possible to create realistic physical simulations for a subset of these food items with the advent of new soft and deformable body simulation capabilities in softwares such as SoFA, NVidia Flex, and Mujoco. For some less deformable food items such as a carrot or a celery or for other food items such as rice which could be simulated as granular media, simulating them can enable an online algorithm to explore the entire action space with a variety of visual and haptic contexts and thus create a dataset with full feedback, which would otherwise be difficult to obtain in real robot experiments. Through this research question, we would like to explore these state-of-the-art advances in simulation platform to create a dataset of a subset of food items that our online learning algorithm can use with a variety of contexts and full feedback, and use this to evaluate how it performs in real world bite acquisition tasks. Note this does not apply to the bite transfer phase, which would require simulating user feedback.

\begin{question} \label{q:rl}
	How should our online algorithm adapt its bite acquisition by considering contexts that affect the ease of bite transfer?
\end{question}
A complete feeding attempt consists of bite acquisition from a plate/bowl and then bite transfer to a user. 
In this question, we explore the validity of \assref{ass:independence} and consider alternative reinforcement learning formalizations which do not require such an assumption.
In a previous work, we found that bite acquisition somewhat affects bite transfer and that how you pick up a food affects how easy it is for a person to take a bite off the fork~\cite{2019Gallenberger}. 
Our preliminary attempt at using online algorithm independently considers contexts that are relevant for successful bite acquisition and transfer. However, for many food items, there are multiple actions that could be successful in picking up a food item but they could affect how easy it is for the person to take the bite off the fork during bite transfer in the future. 

Through this research question, we would like to explore how to incorporate these bite transfer contexts into our decision making process so that our online algorithm considers both success of bite acquisition and ease of bite transfer as rewards to select the actions.
Without \assref{ass:independence}, robot assisted feeding reduces to an episodic Markov Decision Process with two time steps, where the state space is the context space. This will likely require greater exploration during the bite acquisition phase due to sub-optimal acquisition actions causing improved optimal transfer rewards. Fortunately, this may be a practically feasible reinforcement learning problem due to the extremely short time horizon and prior knowledge of the failed acquisition state. We will empirically examine the sample complexity and asymptotic performance loss due to \assref{ass:independence} across several baseline Q-learning algorithms.

